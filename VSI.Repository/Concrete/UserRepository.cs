﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VSI.DB.Context;
using VSI.DB.Models;
using VSI.Repository.Abstract;

namespace VSI.Repository.Concrete
{
    public class UserRepository : IUserRepository
    {
        readonly VSIContext context;

        public UserRepository(VSIContext _context)
        {

            context = _context;

        }
        public int AddUserDetailsInLogin(Login login,ClientRegister register)
        {
         
            context.Database.BeginTransaction();
            context.logins.Add(login);
            context.SaveChanges();
            register.lid = login.lid;
            context.clientRegisters.Add(register);
            context.SaveChanges();
            context.Database.CommitTransaction();
            return login.lid;
            throw new NotImplementedException();
        }

 
        
        public void DeleteUser(string name)
        {
            throw new NotImplementedException();
        }

        public String Login(string username, string password)
        {


            context.Database.BeginTransaction();
         var login =    context.logins.Where(s => s.username == username && s.password == password).FirstOrDefault();

            if (login != null)
            {
                return "Login Success";
            }
            else {
                return "TryAgain";
            }
            throw new NotImplementedException();
        }
    }
}
