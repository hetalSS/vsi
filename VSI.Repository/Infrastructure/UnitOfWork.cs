﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using VSI.DB.Context;
using VSI.Repository.Abstract;
using VSI.Repository.Concrete;

namespace VSI.Repository.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly VSIContext context;
        IDbContextTransaction transaction;
        private bool disposed = false;

        public UnitOfWork(VSIContext _context)
        {
            context = _context;
        }

        public IUserRepository _userRepository { get; set; }
        public IUserRepository userRepository
        {
            get{
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(context);
                
                }
                return _userRepository;
            }
        }
        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public void BeginTransaction()
        {
            if (transaction == null)
                transaction = context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (transaction != null)
                transaction.Commit();
        }

        public void RollbackTransaction()
        {
            try
            {
                if (transaction != null)
                    transaction.Rollback();
            }
            catch (Exception)
            {
                //not required currently
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                context.Dispose();
                if (transaction != null)
                    transaction.Dispose();
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
