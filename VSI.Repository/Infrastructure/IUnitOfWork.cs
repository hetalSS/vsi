﻿using System;
using System.Collections.Generic;
using System.Text;
using VSI.Repository.Abstract;

namespace VSI.Repository.Infrastructure
{
 public  interface IUnitOfWork :IDisposable
    {
        public IUserRepository userRepository { get; }
        int SaveChanges();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
}
