﻿using System;
using System.Collections.Generic;
using System.Text;
using VSI.DB.Models;

namespace VSI.Repository.Abstract
{
     public interface IUserRepository
    {
        public int AddUserDetailsInLogin(Login login, ClientRegister register);
        public String Login(String username, String password);
        public void DeleteUser(String name);
       
        
    }
}
