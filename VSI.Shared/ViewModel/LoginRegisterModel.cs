﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VSI.Shared.ViewModel
{
  public  class LoginRegisterModel 
    {

        public String ClientName { get; set; }
        public String ClientContactNo { get; set; }
        public String ClientDOB { get; set; }
        public String ClientCity { get; set; }
        public String username { get; set; }

        public String password { get; set; }

        public String role { get; set; }
    }
}
