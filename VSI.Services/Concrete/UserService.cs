﻿using System;
using System.Collections.Generic;
using System.Text;
using VSI.DB.Context;
using VSI.DB.Models;
using VSI.Repository.Infrastructure;
using VSI.Services.Abstract;
using VSI.Shared.BaseViewModel;
using VSI.Shared.ViewModel;

namespace VSI.Services.Concrete
{
    public class UserService : IUserServices
    {
        readonly IUnitOfWork unitOfWork;

        public UserService(IUnitOfWork _unitOfWOrk)
        {

            unitOfWork = _unitOfWOrk;
            
        }
      

        public int AddUser(LoginRegisterModel model)
        {
            Login lm = new Login();
            ClientRegister register = new ClientRegister();
            lm.username = model.username;
            lm.password = model.password;
            lm.role = model.role;

            register.ClientName = model.ClientName;
            register.ClientCity = model.ClientCity;
            register.ClientContactNo = model.ClientContactNo;
            register.ClientDOB = model.ClientDOB;

            int lid =   unitOfWork.userRepository.AddUserDetailsInLogin(lm, register);

            return lid;

            throw new NotImplementedException();
        }

        public String checkuser(string username, string password)
        {
           String msg = unitOfWork.userRepository.Login(username, password);

            return msg;
            throw new NotImplementedException();
        }
    }
}
