﻿using System;
using System.Collections.Generic;
using System.Text;
using VSI.Shared.BaseViewModel;
using VSI.Shared.ViewModel;

namespace VSI.Services.Abstract
{
    public   interface IUserServices
    {
        public int AddUser(LoginRegisterModel model);

        public String checkuser(String username,String password);
    }
}
