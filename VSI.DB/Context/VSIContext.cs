﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VSI.DB.Models;

namespace VSI.DB.Context
{
  public  class VSIContext : DbContext
    {
        public VSIContext(DbContextOptions<VSIContext> options)
         : base(options)
        {

        }
        public DbSet<ClientRegister> clientRegisters { get; set; }

        public DbSet<Login> logins { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {


                optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-GDUNVNM\SQLEXPRESS;Initial Catalog=VSIDatabase;Persist Security Info=True;User ID=sa;Password=icreate;");
            }

        }
      

    }
}
