﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VSI.DB.Models
{
   public class Login
    {
        [Key]

        public int lid { get; set; }

        public String username { get; set; }

        public String password { get; set; }

        public String role { get; set; }
    }
}
