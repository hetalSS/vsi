﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VSI.DB.Models
{
  public  class ClientRegister
    {
        [Key]
        public int cid { get;set; }
        public String ClientName { get; set; }
        public  String ClientContactNo { get; set; }
        public String ClientDOB { get; set; }
        public String ClientCity { get; set; }

        [ForeignKey("Login")] 
        public int lid { get; set; }
    }
}
