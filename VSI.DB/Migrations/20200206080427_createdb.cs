﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VSI.DB.Migrations
{
    public partial class createdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "clientRegisters",
                columns: table => new
                {
                    cid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientName = table.Column<string>(nullable: true),
                    ClientContactNo = table.Column<string>(nullable: true),
                    ClientDOB = table.Column<string>(nullable: true),
                    ClientCity = table.Column<string>(nullable: true),
                    lid = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_clientRegisters", x => x.cid);
                });

            migrationBuilder.CreateTable(
                name: "logins",
                columns: table => new
                {
                    lid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    username = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logins", x => x.lid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "clientRegisters");

            migrationBuilder.DropTable(
                name: "logins");
        }
    }
}
