﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VSI.DB.Models;
using VSI.Services.Abstract;
using VSI.Shared.ViewModel;

namespace VSI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        readonly IUserServices userServices;

        public UserController(IUserServices _userservices)
        {

            userServices = _userservices;
        }

        [HttpPost]
        public IActionResult AddUser(LoginRegisterModel model)
        {
           int lid =  userServices.AddUser(model);

            return Ok(lid);
        }
        [HttpPost]
        public IActionResult CheckUser([FromBody] Login login)
        {
            String lid = userServices.checkuser(login.username, login.password);

            return Ok(lid);
        }
    }
}